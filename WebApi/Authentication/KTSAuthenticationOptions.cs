﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace WebApi.Authentication
{
    public class KTSAuthenticationOptions
    {
        public const string Issuer = "KnowledgeTestingSystem_Server";
        public const string Audiense = "KnowledgeTestingSystem_Client";
        const string Key = "s0m3K3yW!thSym_b00ls&";
        public const int Lifetime = 5;
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Key));
        }
    }
}
