﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Interfaces;
using BLL.Models;
using BLL.Validation;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using WebApi.Authentication;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authorization;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IUserService userService;
        public AccountController(IUserService service)
        {
            userService = service;
        }

        /// <summary>
        /// Registers new user
        /// </summary>
        /// <param name="user">User model(formed on client side)</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<ActionResult> Register([FromBody] UserModel user)
        {
            if (!CheckRegistrationData(user.UserName, user.Email, user.Password))
            {
                return BadRequest();
            }
            try
            {
                await userService.AddAsync(user);
            }
            catch (ModelValidationException ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok();
        }

        /// <summary>
        /// Logs in user
        /// </summary>
        /// <param name="data">User data for logging in</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<ActionResult<UserModel>> Login([FromBody] LoginData data)
        {
            string email = data.Email;
            string password = data.Password;
            if (!CheckLoginData(email, password))
            {
                return BadRequest();
            }
            var user = await userService.AuthenticateUser(email, password);
            if (user != null)
            {
                var jwt = await CreateJWT(email, password);
                await userService.SignIn(user);
                return Ok(new { token = jwt });
            }
            return BadRequest();
        }

        /// <summary>
        /// Signs out current user
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("logout")]
        public async Task<ActionResult> Logout()
        {
            await userService.SignOut();
            return Ok();
        }

        /// <summary>
        /// Tests if current user is in specific role, or unauthorized
        /// </summary>
        /// <returns></returns>
        [HttpGet("testRole")]
        public ActionResult Test()
        {
            if (User.IsInRole("Admin"))
            {
                return Ok("You are admin!");
            }
            else if (User.IsInRole("RegisteredUser"))
            {
                return Ok("You are registered user!");
            }
            return Unauthorized("Unauthorized");
        }

        /// <summary>
        /// Method for user changing his data 
        /// </summary>
        /// <param name="userToEdit">Updated userdata</param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        public async Task<ActionResult<UserModel>> Edit([FromBody] UserModel userToEdit)
        {
            // Find current user id
            string userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            if (!Int32.TryParse(userId, out int id))
            {
                return BadRequest();
            }
            // if user tries to edit not himself
            else if (userToEdit.Id != id) //|| !CheckRegistrationData(userToEdit.UserName, userToEdit.Email, userToEdit.Password))
            {
                return BadRequest();
            }
            await userService.UpdateAsync(userToEdit);
            return Ok(userToEdit);
        }

        /// <summary>
        /// Creates JWT for user, according to username and password
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        private async Task<string> CreateJWT(string username, string password)
        {
            UserModel person = await userService.AuthenticateUser(username, password);
            if (person != null)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Email, person.Email),
                    new Claim(ClaimTypes.NameIdentifier, person.Id.ToString())
                };
                foreach (var role in person.Roles)
                {
                    claims.Add(new Claim("Role", role));
                }
                var jwt = new JwtSecurityToken(
                        issuer: KTSAuthenticationOptions.Issuer,
                        audience: KTSAuthenticationOptions.Audiense,
                        notBefore: DateTime.Now,
                        claims: claims,
                        expires: DateTime.Now.Add(TimeSpan.FromMinutes(KTSAuthenticationOptions.Lifetime)),
                        signingCredentials: new SigningCredentials(KTSAuthenticationOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
                var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
                return encodedJwt;
            }
            return null;
        }
        
        /// <summary>
        /// Provides basic validation of registration data
        /// </summary>
        /// <returns>true if data is valid</returns>
        private bool CheckRegistrationData(string userName, string email, string password)
        {
            if (String.IsNullOrEmpty(userName)
                || !userService.CheckEmailAndPassword(email, password))
            {
                return false;
            }
            return true;
        }
        private bool CheckLoginData(string email, string password)
        {
            return userService.CheckEmailAndPassword(email, password);
        }
    }
}
