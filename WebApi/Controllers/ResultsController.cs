﻿using BLL.Interfaces;
using BLL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ResultsController : ControllerBase
    {
        private readonly IResultService resultService;
        public ResultsController(IResultService service)
        {
            resultService = service;
        }
        /// <summary>
        /// Get results of current user
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ResultModel>>> Get()
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            if (!Int32.TryParse(userId, out int id))
            {
                return BadRequest();
            }
            var results = await resultService.GetResultsOfUserAsync(id);
            return Ok(results);
        }

        /// <summary>
        /// Forms test results from user input and adds result to current user
        /// </summary>
        /// <param name="responseOnTest">User response on test</param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<ResultModel>> PassTest([FromBody]ResponseOnTest responseOnTest)
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            if (!Int32.TryParse(userId, out int id))
            {
                return BadRequest();
            }
            responseOnTest.UserId = id;
            ResultModel result = await resultService.FormResultFromInputAsync(responseOnTest);
            await resultService.AddAsync(result);
            return Ok(result);
        }
    }
}
