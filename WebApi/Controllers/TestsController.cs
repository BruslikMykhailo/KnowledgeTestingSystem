﻿using BLL.Interfaces;
using BLL.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestsController : ControllerBase
    {
        readonly ITestCardService testCardService;
        public TestsController(ITestCardService service)
        {
            testCardService = service;
        }
        [HttpGet]
        public ActionResult<IEnumerable<TestCardModel>> Get()
        {
            var tests = testCardService.GetAll();
            return Ok(tests);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TestCardModel>> GetById(int id)
        {
            TestCardModel test =  await testCardService.GetByIdAsync(id);
            return Ok(test);
        }

        [Authorize(Roles = "Admin")]
        [HttpPut("edit")]
        public async Task<ActionResult<TestCardModel>> EditTest([FromBody] TestCardModel testToEdit)
        {
            await testCardService.UpdateAsync(testToEdit);
            var updatedTest = await testCardService.GetByIdAsync(testToEdit.Id);
            return Ok(updatedTest);
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public async void DeleteById(int testId)
        {
            await testCardService.DeleteByIdAsync(testId);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<ActionResult<TestCardModel>> AddNewTest([FromBody] TestCardModel testCardModel)
        {
            await testCardService.AddAsync(testCardModel);
            return Ok();
        }
    }
}
