﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Interfaces;
using BLL.Models;
using BLL.Validation;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using WebApi.Authentication;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authorization;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        readonly IUserService userService;
        public UsersController(IUserService service)
        {
            userService = service;
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public ActionResult<IEnumerable<UserModel>> Get()
        {
            var users = userService.GetAll();
            return Ok(users);
        }

        /// <summary>
        /// Gets user by id
        /// </summary>
        /// <param name="id">Id of desired user</param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<UserModel>>> GetById(int id)
        {
            var user = await userService.GetByIdAsync(id);
            return Ok(user);
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteUser(int id)
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            if (!Int32.TryParse(userId, out int idInInteger))
            {
                return BadRequest();
            }
            else if (id == idInInteger)
            {
                return BadRequest("Cannot delete current user");
            }
            await userService.DeleteByIdAsync(id);
            return Ok();
        }

        
    }
}
