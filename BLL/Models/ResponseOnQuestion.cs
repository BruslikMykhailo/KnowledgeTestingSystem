﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    /// <summary>
    /// Represents user's answer(s) on one question
    /// </summary>
    public class ResponseOnQuestion
    {
        /// <summary>
        /// Id of the question to which the answer was given
        /// </summary>
        public int QuestionId { get; set; }
        /// <summary>
        /// Collection of direct answer(s) on question in string format. If specific answer wasn't selected by user equivalent element of collection should be null or empty string
        /// </summary>
        public List<string> AnswersTexts { get; set; }
    }
}
