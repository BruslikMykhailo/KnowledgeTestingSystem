﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    /// <summary>
    /// Response on test(answers data) obtained from user
    /// </summary>
    public class ResponseOnTest
    {
        /// <summary>
        /// Id of the user who was passing the test
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// Id of the test
        /// </summary>
        public int TestId { get; set; }
        /// <summary>
        /// Amount of time(in seconds) that user spent on test
        /// </summary>
        public int Duration { get; set; }
        /// <summary>
        /// Date and time of finishing the test
        /// </summary>
        public DateTime FinishDate { get; set; }
        /// <summary>
        /// Data of user answers to questions
        /// </summary>
        public List<ResponseOnQuestion> ResponsesData { get; set; }
    }
}
