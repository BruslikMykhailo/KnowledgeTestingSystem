﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    /// <summary>
    /// Category of the test
    /// </summary>
    public class CategoryModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
