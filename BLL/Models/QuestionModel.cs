﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    /// <summary>
    /// Represents question entity from database layer
    /// </summary>
    public class QuestionModel
    {
        public int Id { get; set; }
        public int TestCardId { get; set; }
        public int Points { get; set; }
        public string Text { get; set; }
        public ICollection<AnswerModel> Answers { get; set; }
    }
}
