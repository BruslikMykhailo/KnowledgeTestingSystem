﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    /// <summary>
    /// Filter for tests search
    /// </summary>
    public class TestCardsFilterModel
    {
        /// <summary>
        /// Desired test duration(in seconds)
        /// </summary>
        public int Duration { get; set; }
        /// <summary>
        /// Desired test category
        /// </summary>
        public string Category { get; set; }
    }
}
