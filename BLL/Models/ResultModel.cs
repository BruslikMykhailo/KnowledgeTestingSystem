﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    public class ResultModel
    {
        public int Id { get; set; }
        public int TestCardId { get; set; }
        public int UserId { get; set; }
        public DateTime FinishDate { get; set; }
        /// <summary>
        /// Amount of time(in seconds) that user spent on passing the test
        /// </summary>
        public int Duration { get; set; }
        /// <summary>
        /// Maximum score that was possible at the time of the test passing
        /// </summary>
        public int MaxScore { get; set; }
        /// <summary>
        /// Indicates whether the test passed or not
        /// </summary>
        public int ResultScore { get; set; }
        public bool TestPassed { get; set; }
    }
}
