﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    /// <summary>
    /// Represents TestCard entity from database layer
    /// </summary>
    public class TestCardModel
    {
        public int Id { get; set; }
        public CategoryModel Category { get; set; }
        public string Title { get; set; }
        /// <summary>
        /// Time allotted for the test(in seconds)
        /// </summary>
        public int MaximumDuration { get; set; }
        /// <summary>
        /// Maximum score for the test
        /// </summary>
        public int TotalScore { get; set; }
        /// <summary>
        /// Minimum score to pass the test
        /// </summary>
        public int MinimumScore { get; set; }
        /// <summary>
        /// All questions of the test with possible answers to them
        /// </summary>
        public ICollection<QuestionModel> Questions { get; set; }
    }
}
