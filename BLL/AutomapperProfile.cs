﻿using AutoMapper;
using System.Linq;
using DAL.Entities;
using BLL.Models;


namespace BLL
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<User, UserModel>()
                .ForMember(model => model.TestResultsIds, u => u.MapFrom(user => user.TestResults.Select(res => res.Id)))
                .ReverseMap();
            CreateMap<Answer, AnswerModel>().ReverseMap();
            CreateMap<Question, QuestionModel>()
                .ForMember(q => q.Answers, q => q.MapFrom(question => question.Answers)).ReverseMap();
            CreateMap<Category, CategoryModel>()
                .ForMember(model => model.Name, c => c.MapFrom(category => category.Name)).ReverseMap();
            CreateMap<TestCard, TestCardModel>()
                .ForMember(model => model.Questions, t => t.MapFrom(test => test.Questions))
                .ForMember(model => model.Category, t => t.MapFrom(test => test.Category))
                .ReverseMap();
            CreateMap<TestResult, ResultModel>().ReverseMap();
        }
    }
}
