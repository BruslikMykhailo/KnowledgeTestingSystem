﻿using AutoMapper;
using BLL.Interfaces;
using BLL.Models;
using BLL.Validation;
using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class ResultService : IResultService
    {
        public IUnitOfWork database;
        public IMapper mapper;
        public ResultService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            database = unitOfWork;
            this.mapper = mapper;
        }
        public async Task AddAsync(ResultModel model)
        {
            var result = mapper.Map<TestResult>(model);
            await database.TestResultRepository.AddAsync(result);
            await database.SaveAsync();
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            if (modelId <= 0)
            {
                throw new ModelValidationException("Incorrect model id");
            }
            await database.TestResultRepository.DeleteByIdAsync(modelId);
            await database.SaveAsync();
        }
        
        public async Task<ResultModel> FormResultFromInputAsync(ResponseOnTest responseFromUser)
        {
            ResultModel endResult = new ResultModel
            {
                UserId = responseFromUser.UserId,
                TestCardId = responseFromUser.TestId
            };
            var test = await database.TestCardRepository.GetByIdWithDetailsAsync(responseFromUser.TestId);
            if (responseFromUser.ResponsesData.Count != test.Questions.Count)
            {
                throw new ModelValidationException("Number of answers from input and number of possible answers does not match!");
            }
            int pointsForTest = CalculateScoreForTest(test.Questions, responseFromUser.ResponsesData);
            endResult.MaxScore = test.TotalScore;
            endResult.ResultScore = pointsForTest;
            endResult.TestPassed = pointsForTest >= test.MinimumScore;
            endResult.Duration = responseFromUser.Duration;
            endResult.FinishDate = DateTime.Now;
            return endResult;
        }

        public IEnumerable<ResultModel> GetAll()
        {
            var results = database.TestCardRepository.GetAllWithDetails();
            return mapper.Map<IEnumerable<ResultModel>>(results);
        }

        public async Task<ResultModel> GetByIdAsync(int id)
        {
            if (id <= 0)
            {
                throw new ModelValidationException("Incorrect model id");
            }
            var result = await database.TestResultRepository.GetByIdWithDetailsAsync(id);
            return await Task.Run( () => mapper.Map<ResultModel>(result));
        }

        public async Task UpdateAsync(ResultModel model)
        {
            var testResult = mapper.Map<TestResult>(model);
            await Task.Run(() => database.TestResultRepository.Update(testResult));
            await database.SaveAsync();
        }

        public async Task<IEnumerable<ResultModel>> GetResultsOfUserAsync(int userId)
        {
            var results = database.TestResultRepository.GetAll().Where(result => result.UserId == userId);
            return await Task.Run(() => mapper.Map<IEnumerable<ResultModel>>(results));
        }

        /// <summary>
        /// Counts how many answers the user has selected
        /// </summary>
        /// <returns>Quantity of all selected answers</returns>
        private int CountSelectedAnswers(List<string> answersTexts)
        {
            int selectedCount = 0;
            foreach (var answer in answersTexts)
            {
                if (!String.IsNullOrEmpty(answer))
                {
                    selectedCount++;
                }
            }
            return selectedCount;
        }
        /// <summary>
        /// Calculates test score based on user responses
        /// </summary>
        /// <param name="questionsInTest">List of all questions in the test, with possible answers to them</param>
        /// <param name="userResponsesForQuestions">List of answers selected by user</param>
        /// <returns>Amount of points that user received for test</returns>
        private int CalculateScoreForTest(List<Question> questionsInTest, List<ResponseOnQuestion> userResponsesForQuestions)
        {
            int pointsForTest = 0;
            for (int i = 0; i < questionsInTest.Count; i++)
            {
                int possibleCorrectAnswersCount = 0;
                int usersCorrectAnswers = 0;
                for (int j = 0; j < userResponsesForQuestions[i].AnswersTexts.Count; j++)
                {
                    if (questionsInTest[i].Answers[j].IsCorrect)
                    {
                        possibleCorrectAnswersCount++;
                        if (userResponsesForQuestions[i].AnswersTexts.Any(answer => answer == questionsInTest[i].Answers[j].AnswerText))
                        {
                            usersCorrectAnswers++;
                        }
                    }
                }
                int pointsForQuestion = 0;
                if (usersCorrectAnswers > 0)
                {
                    int usersAllAnswersOnQuestion = CountSelectedAnswers(userResponsesForQuestions[i].AnswersTexts);
                    // If user selected only correct answers calculate points. Otherwise answer considered incorrect.
                    if (usersCorrectAnswers == usersAllAnswersOnQuestion)
                    {
                        pointsForQuestion = (int)((float)questionsInTest[i].Points / ((float)usersCorrectAnswers / (float)possibleCorrectAnswersCount));
                    }
                }
                pointsForTest += pointsForQuestion;
            }
            return pointsForTest;
        }
    }
}
