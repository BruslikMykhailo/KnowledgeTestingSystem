﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using BLL.Interfaces;
using BLL.Models;
using DAL.Entities;
using DAL.Interfaces;

namespace BLL.Services
{
    public class CategoriesService : ICategoriesService
    {
        public IUnitOfWork database;
        public IMapper mapper;
        public CategoriesService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            database = unitOfWork;
            this.mapper = mapper;
        }
        public IEnumerable<CategoryModel> GetAll()
        {
            var categories = database.CategoriesRepository.GetAll();
            return mapper.Map<IEnumerable<CategoryModel>>(categories);
        }
    }
}
