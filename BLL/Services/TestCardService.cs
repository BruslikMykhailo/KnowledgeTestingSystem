﻿using AutoMapper;
using BLL.Interfaces;
using BLL.Models;
using BLL.Validation;
using DAL.Entities;
using DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class TestCardService : ITestCardService
    {
        private readonly IUnitOfWork database;
        private readonly IMapper mapper;
        public TestCardService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            database = unitOfWork;
            this.mapper = mapper;
        }
        public async Task AddAsync(TestCardModel model)
        {
            var test = mapper.Map<TestCard>(model);
            await database.TestCardRepository.AddAsync(test);
            await database.SaveAsync();
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            await database.TestCardRepository.DeleteByIdAsync(modelId);
            await database.SaveAsync();
        }

        public IEnumerable<TestCardModel> GetAll()
        {
            var allTests = database.TestCardRepository.GetAllWithDetails();
            return mapper.Map<IEnumerable<TestCardModel>>(allTests);
        }

        public IEnumerable<TestCardModel> GetByFilter(TestCardsFilterModel filter)
        {
            var testsFiltered = database.TestCardRepository.GetAllWithDetails()
                .Where(test => test.MaximumDuration == filter.Duration 
                || test.Category.Name == filter.Category);
            return mapper.Map<IEnumerable<TestCardModel>>(testsFiltered);
        }

        public async Task<TestCardModel> GetByIdAsync(int id)
        {
            if (id <= 0)
            {
                throw new ModelValidationException("Incorrect test id");
            }
            var test = await database.TestCardRepository.GetByIdWithDetailsAsync(id);
            if (test == null)
            {
                return null;
            }
            return mapper.Map<TestCardModel>(test);
        }

        public async Task UpdateAsync(TestCardModel model)
        {
            var test = mapper.Map<TestCard>(model);
            await Task.Run( () => database.TestCardRepository.Update(test));
            await database.SaveAsync();
        }
    }
}
