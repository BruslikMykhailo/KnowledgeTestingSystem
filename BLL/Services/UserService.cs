﻿using AutoMapper;
using BLL.Interfaces;
using BLL.Models;
using BLL.Validation;
using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class UserService : IUserService
    {
        public const string DefaultUserRole = "RegisteredUser";
        private readonly IUnitOfWork database;
        private readonly IMapper mapper;
        public UserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            database = unitOfWork;
            this.mapper = mapper;
        }
        /// <summary>
        /// Add new user. Used in registration process
        /// </summary>
        /// <param name="model">Model of new user</param>
        /// <returns></returns>
        public async Task AddAsync(UserModel model)
        {
            if (!ValidateModel(model))
            {
                throw new ModelValidationException("User model is not valid!");
            }
            var userInDb = await database.UserManager.FindByEmailAsync(model.Email);
            if (userInDb != null)
            {
                throw new ModelValidationException("This email is already taken!");
            }
            User userToAdd = mapper.Map<User>(model);
            var result = await database.UserManager.CreateAsync(userToAdd, model.Password);
            if (!result.Succeeded)
            {
                throw new ModelValidationException("Something went wrong during registration");
            }
            await database.UserManager.AddToRoleAsync(userToAdd, DefaultUserRole);
            await database.SaveAsync();
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            var userToDelete = await database.UserManager.FindByIdAsync(modelId.ToString());
            await database.UserManager.DeleteAsync(userToDelete);
            await database.SaveAsync();
        }

        public IEnumerable<UserModel> GetAll()
        {
            return mapper.Map<IEnumerable<UserModel>>(database.UserManager.Users);
        }

        /// <summary>
        /// Get users who was passing some test
        /// </summary>
        /// <param name="id">Id of test</param>
        /// <returns>Collection of users who was passing the test</returns>
        public async Task<IEnumerable<UserModel>> GetAllUsersWhoTookTestByTestIdAsync(int id)
        {
            var test = await database.TestCardRepository.GetByIdWithDetailsAsync(id);
            var usersIds = test.TestResults.GroupBy(t => t.UserId).Select(g => g.Key);
            List<User> users = new List<User>();
            foreach (var userId in usersIds)
            {
                users.Add(await database.UserManager.FindByIdAsync(userId.ToString()));
            }
            return await Task.Run(() => mapper.Map<IEnumerable<UserModel>>(users));
        }

        public async Task<UserModel> GetByIdAsync(int id)
        {
            var user = await database.UserManager.FindByIdAsync(id.ToString());
            if (user == null)
            {
                throw new ModelValidationException("Invalid user ID");
            }
            return mapper.Map<UserModel>(user);
        }

        /// <summary>
        /// Searches user by email and returns user model. Throws exception if user with such e,ail was not found
        /// </summary>
        /// <param name="email">Email of specific user</param>
        /// <returns>User model if user find successfully</returns>
        public async Task<UserModel> GetByEmailAsync(string email)
        {
            var user = await database.UserManager.FindByEmailAsync(email);
            if (user == null)
            {
                throw new ModelValidationException($"Cannot find user with email: {email}");
            }
            return mapper.Map<UserModel>(user);
        }

        public async Task UpdateAsync(UserModel model)
        {
            User userInDb = await database.UserManager.FindByIdAsync(model.Id.ToString());
            userInDb.UserName = model.UserName;
            userInDb.Email = model.Email;
            await database.UserManager.UpdateAsync(userInDb);
            await database.SaveAsync();
        }

        /// <summary>
        /// Returns list of all roles of user
        /// </summary>
        /// <param name="userId">Id of the user</param>
        /// <returns></returns>
        public async Task<IEnumerable<string>> GetUserRolesAsync(int userId)
        {
            User user = await database.UserManager.FindByIdAsync(userId.ToString());
            if (user == null)
            {
                throw new ModelValidationException("Incorrect user id!");
            }
            return await database.UserManager.GetRolesAsync(user);
        }

        /// <summary>
        /// Checks if user has role.
        /// </summary>
        /// <param name="userId">Id of the user</param>
        /// <param name="role">Role name</param>
        /// <returns>True if user is assosiated with such role, otherwise false</returns>
        public async Task<bool> CheckUserIsInRoleAsync(int userId, string role)
        {
            User user = await database.UserManager.FindByIdAsync(userId.ToString());
            if (user == null)
            {
                throw new ModelValidationException("Incorrect user id!");
            }
            return await database.UserManager.IsInRoleAsync(user, role);
        }

        /// <summary>
        /// Provides user authentication
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns>User model on successful authentication and null on unsuccessful</returns>
        public async Task<UserModel> AuthenticateUser(string email, string password)
        {
            User user = await database.UserManager.FindByEmailAsync(email);
            if (user == null)
            {
                return null;
            }
            var signInResult = await database.SignInManager.CheckPasswordSignInAsync(user, password, false);
            if (!signInResult.Succeeded)
            {
                return null;
            }
            UserModel model = mapper.Map<UserModel>(user);
            model.Roles = await database.UserManager.GetRolesAsync(user);
            return model;
        }

        /// <summary>
        /// Signs in specified user
        /// </summary>
        /// <param name="user">Model of the user who logs into the account</param>
        /// <returns></returns>
        public async Task SignIn(UserModel user)
        {
            User userEntity = await database.UserManager.FindByEmailAsync(user.Email);
            await database.SignInManager.SignInAsync(userEntity, false);
        }

        /// <summary>
        /// Signs out current user
        /// </summary>
        /// <returns></returns>
        public async Task SignOut()
        {
            await database.SignInManager.SignOutAsync();
        }

        /// <summary>
        /// Provides basic validation of user model
        /// </summary>
        /// <param name="model">Specific user model</param>
        /// <returns>True if model is valid. False if model is invalid</returns>
        private bool ValidateModel(UserModel model)
        {
            return CheckEmailAndPassword(model.Email, model.Password);
            // TODO validation of other user properties(username, id?)
        }
        /// <summary>
        /// Provides basic validation of email data password according to default identity options
        /// </summary>
        /// <returns>True if data is valid</returns>
        public bool CheckEmailAndPassword(string email, string password)
        {
            // check if values are empty
            if (String.IsNullOrEmpty(email) || String.IsNullOrEmpty(password))
            {
                return false;
            }
            // password check
            Regex regex = new Regex(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[_#$^+=!*()@%&]).{6,}$");
            if (!regex.IsMatch(password))
            {
                return false;
            }
            // email check
            try
            {
                MailAddress m = new MailAddress(email);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}
