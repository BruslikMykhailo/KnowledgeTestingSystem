﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using DAL.Entities;
using BLL.Models;

namespace BLL.Interfaces
{
    public interface IStatisticsService
    {
        IEnumerable<UserModel> GetUsersWhoPassedTheMostTests(int usersCount, DateTime firstDate, DateTime lastDate);
        int GetAverageTestScoreById(int testId);
    }
}
