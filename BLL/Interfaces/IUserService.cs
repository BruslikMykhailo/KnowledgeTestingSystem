﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.Models;

namespace BLL.Interfaces
{
    public interface IUserService : ICrud<UserModel>
    {
        /// <summary>
        /// Get users who was passing some test
        /// </summary>
        /// <param name="id">Id of test</param>
        /// <returns>Collection of users who was passing the test</returns>
        Task<IEnumerable<UserModel>> GetAllUsersWhoTookTestByTestIdAsync(int id);
        /// <summary>
        /// Searches user by email and returns user model. Throws exception if user with such e,ail was not found
        /// </summary>
        /// <param name="email">Email of specific user</param>
        /// <returns>User model if user find successfully</returns>
        Task<UserModel> GetByEmailAsync(string email);
        /// <summary>
        /// Checks if user has role.
        /// </summary>
        /// <param name="userId">Id of the user</param>
        /// <param name="role">Role name</param>
        /// <returns>True if user is assosiated with such role, otherwise false</returns>
        Task<bool> CheckUserIsInRoleAsync(int userId, string role);
        Task<IEnumerable<string>> GetUserRolesAsync(int userId);
        /// <summary>
        /// Provides user authentication
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns>User model on successful authentication and null on unsuccessful</returns>
        Task<UserModel> AuthenticateUser(string email, string password);
        /// <summary>
        /// Signs in specified user
        /// </summary>
        /// <param name="user">Model of the user who logs into the account</param>
        /// <returns></returns>
        Task SignIn(UserModel user);
        /// <summary>
        /// Signs out current user
        /// </summary>
        /// <returns></returns>
        Task SignOut();
        /// <summary>
        /// Provides basic validation of email data password according to default identity options
        /// </summary>
        /// <returns>True if data is valid</returns>
        bool CheckEmailAndPassword(string email, string password);
    }
}
