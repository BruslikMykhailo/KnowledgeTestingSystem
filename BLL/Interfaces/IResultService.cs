﻿using BLL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IResultService : ICrud<ResultModel>
    {
        /// <summary>
        /// Forms resultModel object from user input
        /// </summary>
        /// <param name="resultFromInput"></param>
        /// <returns></returns>
        Task<ResultModel> FormResultFromInputAsync(ResponseOnTest resultFromInput);
        /// <summary>
        /// Get all results of specific user
        /// </summary>
        /// <param name="userId">User id</param>
        /// <returns>Collection of results or null if there is no results in db.</returns>
        Task<IEnumerable<ResultModel>> GetResultsOfUserAsync(int userId);
    }
}
