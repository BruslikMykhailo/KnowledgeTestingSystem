﻿using BLL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Interfaces
{
    public interface ITestCardService : ICrud<TestCardModel>
    {
        public IEnumerable<TestCardModel> GetByFilter(TestCardsFilterModel filter);
    }
}
