﻿using System.Linq;
using System.Threading.Tasks;
using DAL.Entities;

namespace DAL.Interfaces
{
    public interface ITestResultRepository : IRepository<TestResult>
    {
        IQueryable<TestResult> GetAllWithDetails();
        Task<TestResult> GetByIdWithDetailsAsync(int id);
    }
}
