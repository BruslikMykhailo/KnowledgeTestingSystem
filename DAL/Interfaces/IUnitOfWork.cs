﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using DAL.Entities;

namespace DAL.Interfaces
{
    public interface IUnitOfWork
    {
        UserManager<User> UserManager { get; }
        RoleManager<Role> RoleManager { get; }
        SignInManager<User> SignInManager { get; }
        IRepository<Category> CategoriesRepository { get; }
        ITestCardRepository TestCardRepository { get; }
        ITestResultRepository TestResultRepository { get; }
        Task<int> SaveAsync();
    }
}
