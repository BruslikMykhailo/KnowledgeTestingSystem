﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface ITestCardRepository : IRepository<TestCard>
    {
        IQueryable<TestCard> GetAllWithDetails();
        Task<TestCard> GetByIdWithDetailsAsync(int id);
    }
}
