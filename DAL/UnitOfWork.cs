﻿using DAL.Entities;
using DAL.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DAL.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private KTSContext dbContext;

        private UserManager<User> userManager;
        private RoleManager<Role> roleManager;
        private SignInManager<User> signInManager;
        private CategoriesRepository categoriesRepository;
        private TestCardRepository testCardRepository;
        private TestResultRepository testResultRepository;

        public UnitOfWork(DbContextOptions<KTSContext> options, UserManager<User> userManager, RoleManager<Role> roleManager, SignInManager<User> signInManager)
        {
            dbContext = new KTSContext(options);
            this.userManager = userManager;
            this.roleManager = roleManager;
            this.signInManager = signInManager;
        }
        public UserManager<User> UserManager
        {
            get
            {
                return userManager;
            }
        }

        public RoleManager<Role> RoleManager
        {
            get
            {
                return roleManager;
            }
        }

        public SignInManager<User> SignInManager
        {
            get
            {
                return signInManager;
            }
        }

        public IRepository<Category> CategoriesRepository
        {
            get
            {
                if (categoriesRepository == null)
                {
                    categoriesRepository = new CategoriesRepository(dbContext);
                }
                return categoriesRepository;
            }
        }

        public ITestCardRepository TestCardRepository
        {
            get
            {
                if (testCardRepository == null)
                {
                    testCardRepository = new TestCardRepository(dbContext);
                }
                return testCardRepository;
            }
        }

        public ITestResultRepository TestResultRepository
        {
            get
            {
                if (testResultRepository == null)
                {
                    testResultRepository = new TestResultRepository(dbContext);
                }
                return testResultRepository;
            }
        }

        public async Task<int> SaveAsync()
        {
            return await dbContext.SaveChangesAsync();
        }
    }
}
