﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class TestCategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CategoryTestCard");

            migrationBuilder.AddColumn<int>(
                name: "CategoryId",
                table: "TestCards",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TestCards_CategoryId",
                table: "TestCards",
                column: "CategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_TestCards_Categories_CategoryId",
                table: "TestCards",
                column: "CategoryId",
                principalTable: "Categories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TestCards_Categories_CategoryId",
                table: "TestCards");

            migrationBuilder.DropIndex(
                name: "IX_TestCards_CategoryId",
                table: "TestCards");

            migrationBuilder.DropColumn(
                name: "CategoryId",
                table: "TestCards");

            migrationBuilder.CreateTable(
                name: "CategoryTestCard",
                columns: table => new
                {
                    CategoriesId = table.Column<int>(type: "int", nullable: false),
                    TestCardsId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoryTestCard", x => new { x.CategoriesId, x.TestCardsId });
                    table.ForeignKey(
                        name: "FK_CategoryTestCard_Categories_CategoriesId",
                        column: x => x.CategoriesId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CategoryTestCard_TestCards_TestCardsId",
                        column: x => x.TestCardsId,
                        principalTable: "TestCards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CategoryTestCard_TestCardsId",
                table: "CategoryTestCard",
                column: "TestCardsId");
        }
    }
}
