﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class User : IdentityUser<int>
    {
        public virtual ICollection<TestResult> TestResults { get; set; } 
    }
}
