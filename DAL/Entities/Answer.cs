﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    /// <summary>
    /// Answer to a specific question
    /// </summary>
    public class Answer
    {
        public int Id { get; set; }
        /// <summary>
        /// Id of the question with which this answer is associated
        /// </summary>
        public int QuestionId { get; set; }
        /// <summary>
        /// Indicates wether this answer correct or not
        /// </summary>
        public bool IsCorrect { get; set; }
        /// <summary>
        /// Text of the answer
        /// </summary>
        public string AnswerText { get; set; }
        public virtual Question Question { get; set; }
    }
}
