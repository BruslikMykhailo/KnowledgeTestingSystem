﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class Question
    {
        public int Id { get; set; }
        public int TestCardId { get; set; }
        /// <summary>
        /// Points for correct answering on a question
        /// </summary>
        public int Points { get; set; }
        /// <summary>
        /// Text of the question
        /// </summary>
        public string Text { get; set; }
        public virtual List<Answer> Answers { get; set; }
        public virtual TestCard TestCard { get; set; }
    }
}
