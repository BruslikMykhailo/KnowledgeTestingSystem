﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    /// <summary>
    /// Category of tests
    /// </summary>
    public class Category
    {
        public int Id { get; set; }
        /// <summary>
        /// Name of the category
        /// </summary>
        public string Name { get; set; }
        public virtual ICollection<TestCard> TestCards { get; set; }
    }
}
