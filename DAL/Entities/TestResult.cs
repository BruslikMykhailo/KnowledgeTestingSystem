﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class TestResult
    {
        public int Id { get; set; }
        public int TestCardId { get; set; }
        public int UserId { get; set; }
        /// <summary>
        /// Amount of time(in seconds) the user spent on passing the test
        /// </summary>
        public int Duration { get; set; }
        /// <summary>
        /// Date and time of finishing the test
        /// </summary>
        public DateTime FinishDate { get; set; }
        public int ResultScore { get; set; }
        /// <summary>
        /// Maximum score that was possible at the time of the test passing
        /// </summary>
        public int MaxScore { get; set; }
        /// <summary>
        /// Indicates whether the test passed or not
        /// </summary>
        public bool TestPassed { get; set; }
        public virtual TestCard TestCard { get; set; }
        public virtual User User { get; set; }
    }
}
