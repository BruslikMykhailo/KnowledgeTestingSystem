﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class TestCard
    {
        public int Id { get; set; }
        /// <summary>
        /// Title of the test
        /// </summary>
        public string Title { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? DateOfChange { get; set; }
        /// <summary>
        /// Maximum test duration(in seconds)
        /// </summary>
        public int MaximumDuration { get; set; }
        /// <summary>
        /// Maximum score for the test. Should be calculated as sum of points for each question in test
        /// </summary>
        public int TotalScore { get; set; }
        /// <summary>
        /// Minimum score to pass the test
        /// </summary>
        public int MinimumScore { get; set; }

        public virtual Category Category { get; set; }
        public virtual List<Question> Questions { get; set; }
        public virtual List<TestResult> TestResults { get; set; }

    }
}
