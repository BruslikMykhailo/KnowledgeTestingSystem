﻿using DAL.Interfaces;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace DAL.Repositories
{
    public class CategoriesRepository : IRepository<Category>
    {
        private readonly KTSContext dbContext;
        public CategoriesRepository(KTSContext context)
        {
            dbContext = context;
        }
        public async Task AddAsync(Category entity)
        {
            await dbContext.Categories.AddAsync(entity);
        }

        public void Delete(Category entity)
        {
            dbContext.Categories.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            Category categoryToDelete = await dbContext.Categories.FindAsync(id);
            await Task.Run(() => dbContext.Categories.Remove(categoryToDelete));
        }

        public IQueryable<Category> GetAll()
        {
            return dbContext.Categories;
        }

        public async Task<Category> GetByIdAsync(int id)
        {
            return await dbContext.Categories.FindAsync(id);
        }

        public void Update(Category entity)
        {
            dbContext.Categories.Update(entity);
        }
    }
}
