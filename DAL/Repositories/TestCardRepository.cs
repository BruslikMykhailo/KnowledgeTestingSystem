﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class TestCardRepository : ITestCardRepository
    {
        private readonly KTSContext dbContext;
        public TestCardRepository(KTSContext context)
        {
            dbContext = context;
        }

        public async Task AddAsync(TestCard entity)
        {
            entity.CreationDate = DateTime.Now;
            await dbContext.TestCards.AddAsync(entity);
        }

        public void Delete(TestCard entity)
        {
            dbContext.TestCards.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            TestCard cardToDelete = await dbContext.TestCards.FindAsync(id);
            await Task.Run(() => dbContext.TestCards.Remove(cardToDelete));
        }

        public IQueryable<TestCard> GetAll()
        {
            return dbContext.TestCards;
        }

        /// <summary>
        /// Returns all TestCards, and loads in context all questions and answers
        /// </summary>
        /// <returns></returns>
        public IQueryable<TestCard> GetAllWithDetails()
        {
            return dbContext.TestCards.Include(test => test.Category)
                .Include(test => test.TestResults)
                .Include(test => test.Questions)
                .ThenInclude(question => question.Answers);
        }

        public async Task<TestCard> GetByIdAsync(int id)
        {
            return await dbContext.TestCards.FindAsync(id);
        }

        /// <summary>
        /// Returns TestCard by desired id, and loads in context questions and answers related to this test
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<TestCard> GetByIdWithDetailsAsync(int id)
        {
            return await dbContext.TestCards.Include(test => test.Category)
                .Include(test => test.TestResults)
                .Include(test => test.Questions)
                .ThenInclude(question => question.Answers)
                .FirstOrDefaultAsync(test => test.Id == id);
        }

        /// <summary>
        /// Updates testCard entity. If changes was made in quantity of questions  
        /// reloads questions them(with deliting of unused questions)
        /// </summary>
        /// <param name="entityFromModel"></param>
        public void Update(TestCard entityFromModel)
        {
            var existingEntity = dbContext.TestCards
                .Where(p => p.Id == entityFromModel.Id)
                .Include(p => p.Questions)
                .SingleOrDefault();

            if (existingEntity != null)
            {
                // Update parent
                dbContext.Entry(existingEntity).CurrentValues.SetValues(entityFromModel);

                // Delete children
                foreach (var existingChild in existingEntity.Questions.ToList())
                {
                    if (!entityFromModel.Questions.Any(c => c.Id == existingChild.Id))
                        dbContext.Questions.Remove(existingChild);
                }

                // Update and Insert children
                foreach (var childQuestion in entityFromModel.Questions)
                {
                    var existingQuestion = existingEntity.Questions
                        .Where(c => c.Id == childQuestion.Id && c.Id != 0)
                        .SingleOrDefault();

                    if (existingQuestion != null)
                    {
                        // Update child
                        UpdateQuestion(childQuestion);
                    }
                    else
                    {
                        existingEntity.Questions.Add(childQuestion);
                    }
                }
            }
            int overallPoints = 0;
            foreach (var question in existingEntity.Questions)
            {
                overallPoints += question.Points;
            }
            existingEntity.TotalScore = overallPoints;
            existingEntity.DateOfChange = DateTime.Now;
        }

        /// <summary>
        /// Updates question entity. If changes was made in quantity of child answers  
        /// reloads answers them(with deliting of unused answers)
        /// </summary>
        /// <param name="questionFromModel"></param>
        public void UpdateQuestion(Question questionFromModel)
        {
            var existingQuestion = dbContext.Questions
                .Where(p => p.Id == questionFromModel.Id)
                .Include(p => p.Answers)
                .SingleOrDefault();

            if (existingQuestion != null)
            {
                dbContext.Entry(existingQuestion).CurrentValues.SetValues(questionFromModel);

                foreach (var existingChild in existingQuestion.Answers.ToList())
                {
                    if (!questionFromModel.Answers.Any(c => c.Id == existingChild.Id))
                        dbContext.Answers.Remove(existingChild);
                }

                foreach (var childModel in questionFromModel.Answers)
                {
                    var existingChild = existingQuestion.Answers
                        .Where(c => c.Id == childModel.Id && c.Id != default(int))
                        .SingleOrDefault();

                    if (existingChild != null)
                    {
                        dbContext.Entry(existingChild).CurrentValues.SetValues(childModel);
                    }
                    else
                    {
                        existingQuestion.Answers.Add(childModel);
                    }
                }
            }
        }
    }
}
