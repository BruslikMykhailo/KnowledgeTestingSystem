﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class TestResultRepository : ITestResultRepository
    {
        private readonly KTSContext dbContext;
        public TestResultRepository(KTSContext context)
        {
            dbContext = context;
        }
        public async Task AddAsync(TestResult entity)
        {
            await dbContext.TestResults.AddAsync(entity);
        }

        public void Delete(TestResult entity)
        {
            dbContext.TestResults.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            TestResult cardToDelete = await dbContext.TestResults.FindAsync(id);
            await Task.Run(() => dbContext.TestResults.Remove(cardToDelete));
        }

        public IQueryable<TestResult> GetAll()
        {
            return dbContext.TestResults;
        }

        /// <summary>
        /// Returns results of all test, and loads to context users and test.
        /// </summary>
        /// <returns></returns>
        public IQueryable<TestResult> GetAllWithDetails()
        {
            return dbContext.TestResults.Include(result => result.User)
                .Include(result => result.TestCard);
        }

        public async Task<TestResult> GetByIdAsync(int id)
        {
            return await dbContext.TestResults.FindAsync(id);
        }

        public async Task<TestResult> GetByIdWithDetailsAsync(int id)
        {
            return await dbContext.TestResults.Include(result => result.User)
                .Include(result => result.TestCard)
                .FirstOrDefaultAsync(result => result.Id == id);
        }

        public void Update(TestResult entity)
        {
            dbContext.TestResults.Update(entity);
        }
    }
}
