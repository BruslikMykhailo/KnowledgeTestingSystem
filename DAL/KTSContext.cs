﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL
{
    public class KTSContext : IdentityDbContext<User, Role, int>
    {
        public KTSContext(DbContextOptions<KTSContext> options) : base(options) 
        {
            
        }
        public DbSet<TestCard> TestCards { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<TestResult> TestResults { get; set; }
    }
}
